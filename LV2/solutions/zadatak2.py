import re

#1. sadrži najmanje jedno slovo a
#2. sadrži točno jedno slovo a
#3. ne sadrži slovo a
#4. sadrži jedan ili više numeričkih znakova (0 – 9)
#5. sadrži samo mala slova (a-z) 

try:
    txt = open(r"C:\Users\student\Desktop\rusu_lv_2019_2020\LV2\solutions\mbox-short.txt", 'r')
except:
    print("Error")
    exit()
    
mails = []
for line in txt:
    mails.extend(re.findall('\S+@\S+', line))
    
for idx in range(len(mails)):
    mails[idx]=mails[idx].split('@')[0]
    mails[idx]=mails[idx].replace('<', '')
    
mails = list(set(mails))

mails1 = []
mails2 = []
mails3 = []
mails4 = []
mails5 = []

reg1 = re.compile('(\S*a\S*.)+')
reg2 = re.compile('^[^a]*a[^a]*$')
reg3 = re.compile('^[^a]+$')
reg4 = re.compile('[0-9]+')
reg5 = re.compile('^[a-z]+$')

for mail in mails:
    if(reg1.match(mail)):
        mails1.append(mail)
    if(reg2.match(mail)):
        mails2.append(mail)
    if(reg3.match(mail)):
        mails3.append(mail)
    if(reg4.match(mail)):
        mails4.append(mail)
    if(reg5.match(mail)):
        mails5.append(mail)
        
print(mails)
print(mails1)
print(mails2)
print(mails3)
print(mails4)
print(mails5)


