import numpy as np
import matplotlib.pyplot as plt

#Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1. Neka 1
#označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem
#odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
#standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm
#i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) odnosno crvenom bojom (0). Napišite
#funkciju koja računa srednju vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
#np.dot). Prikažite i dobivene srednje vrijednosti na grafu.

def assign_height(i):
    if(i == 1):
        return np.random.normal(180,7)
    return np.random.normal(167,7)

gender = np.random.randint(2, size=1000)
height = list(map(assign_height,gender))
assigned = [(gender[i], height[i]) for i in range(0, len(gender))]
height_m = [a[1] for a in assigned if a[0] == 1]
height_f = [a[1] for a in assigned if a[0] == 0]
avg_m = sum(height_m)/len(height_m)
avg_f = sum(height_f)/len(height_f)
plt.hist(height_m, 50, color=(0,0,1,0.5))
plt.hist(height_f, 50, color=(1,0,0,0.5))
plt.axvline(avg_m, color='blue')
plt.axvline(avg_f, color='red')

