import re

try:
    txt = open(r"C:\Users\student\Desktop\rusu_lv_2019_2020\LV2\solutions\mbox-short.txt", 'r')
except:
    print("Error")
    exit()
    
mails = []
for line in txt:
    mails.extend(re.findall('\S+@\S+', line))
    
for idx in range(len(mails)):
    mails[idx]=mails[idx].split('@')[0]
    mails[idx]=mails[idx].replace('<', '')
mails = list(set(mails))
print(mails)