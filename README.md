# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.

Važno je napomenuti da neki commitovi su pod tuđim imenom zbog globalnih postavki na računalima na labosu.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime: Dijana Ivezić
