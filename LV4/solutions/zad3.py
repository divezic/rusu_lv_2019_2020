#!/usr/bin/env python
# coding: utf-8

# In[35]:


#Na podacima iz zadatka 1 potrebno je odrediti parametre linearnog modela na podacima za učenje na način da se
#implementira funkcija za izračunavanje parametara linearnog modela metodom gradijentnog spusta. Usporedite dobivene
#parametre modela s vrijednostima parametara linearnog modela iz zadatka 1.
#Slikom prikažite vrijednosti kriterijske funkcije u svakoj iteraciji algoritma. Mijenjate duljinu koraka α od vrlo malih
#vrijednosti do vrlo velikih vrijednosti. Što se događa?

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
 y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
 return y
def add_noise(y):
 np.random.seed(14)
 varNoise = np.max(y) - np.min(y)
 y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
 return y_noisy

x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]
x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]
xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]
xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)


# In[50]:


theta = [0,0]
alpha = 0.01
for j in range(10000):
    dt1 = 0
    dt0 = 0
    for i in range(len(xtrain)):
        dt1 = dt1 + ((theta[0] * xtrain[i] + theta[1])-ytrain[i]) * xtrain[i]
        dt0 = dt0 + ((theta[0] * xtrain[i] + theta[1])-ytrain[i]) * 1
    dt1 = (1)/(len(xtrain)) * dt1
    dt0 = (1)/(len(xtrain)) * dt0
    theta[0] = theta[0] - alpha * dt1
    theta[1] = theta[1] - alpha * dt0
print(theta)


# In[15]:


linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)
print('Model je oblika y_hat = Theta0 + Theta1 * x')
print('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)
plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)


# In[ ]:




