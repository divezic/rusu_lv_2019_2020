#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')


# In[2]:


print(mtcars)


# In[3]:


#Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
mtcars_highest_mpg = (mtcars.sort_values('mpg')).head(5)
print(mtcars_highest_mpg)


# In[4]:


#Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
mtcars_8cil = (mtcars[mtcars.cyl == 8].sort_values('mpg', ascending=False)).head(3)
print(mtcars_8cil)


# In[5]:


#Kolika je srednja potrošnja automobila sa 6 cilindara?
mtcars_6cil= mtcars[mtcars.cyl == 6].mpg
mtcars_6cil_sum = mtcars_6cil.sum()
mtcars_6cil_cnt = mtcars_6cil.count()
mtcars_6cil_avg = mtcars_6cil_sum/mtcars_6cil_cnt
print(mtcars_6cil_avg)


# In[6]:


#Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
mtcars_4cil= mtcars[(mtcars.cyl == 4)]
wt = (mtcars.wt >= 2.000) & (mtcars.wt <= 2.200)
mtcars_4cil_wt = mtcars_4cil[wt].mpg
mtcars_4cil_wt_sum = mtcars_4cil_wt.sum()
mtcars_4cil_wt_cnt = mtcars_4cil_wt.count()
mtcars_4cil_wt_avg = mtcars_4cil_wt_sum/mtcars_4cil_wt_cnt
print(mtcars_4cil_wt_avg)


# In[7]:


#Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
mtcars_manual = (mtcars[mtcars.am==0].am).count()
mtcars_automatic = (mtcars[mtcars.am==1].am).count()
print(mtcars_manual)
print(mtcars_automatic)


# In[8]:


#Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
mtcars_automatic_hp = mtcars[(mtcars.am==1) & (mtcars.hp > 100)].car.count()
print(mtcars_automatic_hp)


# In[10]:


#Kolika je masa svakog automobila u kilogramima? 
mtcars_car = mtcars.car
mtcars_wt = mtcars.wt
mtcars_wt = 0.45359237*mtcars_wt
mtcars_car['kg'] = mtcars_wt
print(mtcars_car)


# In[ ]:




