#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')


# In[17]:


#Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara. 
mpg_4 = mtcars[mtcars.cyl == 4].mpg
mpg_6 = mtcars[mtcars.cyl == 6].mpg
mpg_8 = mtcars[mtcars.cyl == 8].mpg
mpg_4.plot.bar(color=(1, 0, 0, 0.2))
mpg_6.plot.bar(color=(0,0,1,0.2))
mpg_8.plot.bar(color=(0,1,0,0.2))


# In[28]:


#Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara
wt_4 = mtcars[mtcars.cyl == 4]
wt_6 = mtcars[mtcars.cyl == 6]
wt_8 = mtcars[mtcars.cyl == 8]
wt_4.boxplot(column='wt', color='red')
wt_6.boxplot(column='wt',color='blue')
wt_8.boxplot(column='wt', color='green')


# In[31]:


#Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
#potrošnju od automobila s automatskim mjenjačem?
mpg_0 = mtcars[mtcars.am == 0]
mpg_1 = mtcars[mtcars.am == 1]
mpg_0.boxplot('mpg', color='red')
mpg_1.boxplot('mpg')


# In[51]:


#Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
#mjenjačem.
qsec_0 = (mtcars[mtcars.am == 0])[['qsec', 'hp']].sort_values(by='hp')
qsec_1 = (mtcars[mtcars.am == 1])[['qsec', 'hp']].sort_values(by='hp')
print(qsec_0)
qsec_0.plot(x='hp', y='qsec')
qsec_1.plot(x='hp', y='qsec')


# In[ ]:




