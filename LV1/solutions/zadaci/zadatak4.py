def print_values(nums):
    avg = sum(nums)/len(nums)
    mi = min(nums)
    ma = max(nums)
    print('Srednja vrijednost: ', avg)
    print('Minimalna vrijednost: ', mi)
    print('Maksimalna vrijednost: ', ma)

nums = []

while(True):
    try:
        i =input('Vaš unos: ')
        i = float(i)
        nums.append(i)
    except ValueError:
        i = i.lower()
        if(i == 'done'):
            print_values(nums)
            exit()
        else:
            print('To nije broj.')
             