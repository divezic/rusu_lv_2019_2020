try:
    broj = float(input('Unesite broj od 0 do 1...'))
except:
    print('To nije broj')
    exit()
    
if(broj <0.0 or broj >1.0):
    print('Broj se ne nalazi u zadanom intervalu.')
else:
    if(broj >= 0.9):
        print('A')
    elif(broj >= 0.8):
        print('B')
    elif(broj >= 0.7):
        print('C')
    elif(broj >= 0.6):
        print('D')
    else:
        print('F')
