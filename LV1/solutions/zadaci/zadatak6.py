try:
    file = open('mbox-short.txt', 'r')
except:
    print('Nema mbox-short.txt datoteke.')
    exit()
    
di = dict()

for line in file:
    if(line.startswith('From: ')):
        temp = line
        temp = temp.replace('From: ', '')
        temp = temp.replace('\n', '')
        i = temp.find('@')
        temp = temp[i+1:]
        if temp not in di:
            di[temp] = 1
        else:
            di[temp] = di[temp] + 1
            
print(di)